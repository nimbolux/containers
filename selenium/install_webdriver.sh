#!/bin/bash

INSTALL_DIR="/bin/"

json=$(curl -s https://api.github.com/repos/mozilla/geckodriver/releases/latest)
url=$(echo "$json" | jq -r '.assets[].browser_download_url | select(endswith("linux64.tar.gz"))')
curl -s -L "$url" | tar -xz
chmod +x geckodriver
mv geckodriver "$INSTALL_DIR"

apt install chromium-driver
